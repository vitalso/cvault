$(function(){

	// Scroll to section
	$('.intro .secondary-btn').on('click' , function(e){
		e.preventDefault();
		var hash = $(this).attr('href');
    
    	$('html, body').animate({scrollTop: $(hash).offset().top}, 800);
	})

	// Header toggle class when scroll
	$(window).on('scroll' , function(){
		if ( $(window).scrollTop() > $('header').height() ) {
			$('header').addClass('fixed');
		} else {
			$('header').removeClass('fixed');
		}
	});

	// Toogle class button menu
	$('.navbar-toggler').on('click' , function(){
		$(this).toggleClass('open');
		$(this).closest('header').toggleClass('open');
	})

	// FAQ
	$('.panel-heading').on('click' , function(){
		$(this).toggleClass('open');
	});

	// Popup youtube
	$('.play-button').magnificPopup({
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});

	// Partners logo slider
	$('.partners-logo').slick({
		dots: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
	    {
	    	breakpoint: 768,
	      		settings: {
	        		slidesToShow: 2
	      		}
	    },
	    {
	    	breakpoint: 480,
	      		settings: {
	        		slidesToShow: 1
	      		}
	    }
	  ]
  	});

});